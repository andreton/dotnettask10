﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask10
{

    class Bird : Animal //Bird is a child class of animal
    {
        private string wingspan { get; set; } // Adding wingspan to the bird class

        public Bird(string name, string orginCountry, string color) //Standard constructor from animal class
          : base(name, orginCountry, color)
        {

        }

        public Bird(string name, string orginCountry, string color, string wingspan) //Inherit the constructor from the main animal class, overloads with wingspan
            : base(name, orginCountry, color)
        {
            this.wingspan = wingspan;
        }
        public override string ToString() //Making a tostring method for the additional wingspan, if wingspan is not defined, only call toString method
        {
            if (wingspan == null)
            {
                return base.ToString();
            }
            else
            {
                return base.ToString() + " wingspan " + this.wingspan;
            }

        }

        public override void Move() //Implementation of the interface method 
        {
            Console.WriteLine("I like to fly");
        }
    }
}