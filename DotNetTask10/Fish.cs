﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
namespace DotNetTask10
{
    class Fish : Animal,IMovement
    {
        private string size { get; set; } // Adding size to the fish class

        public Fish(string name, string orginCountry, string color) //Standard constructor from animal class
          : base(name, orginCountry, color)
        {}

        public Fish(string name, string orginCountry, string color, string size) //Inherit the constructor from the main animal class, overloads with size
            : base(name, orginCountry, color)
        {
            this.size = size;
        }
        public override string ToString() //Making a tostring method for the size
        {
            if (size == null)
            {
                return base.ToString(); //Calling base constructor if no size is given
            }
            else
            {
                return base.ToString() + " size: " + this.size;// adding size to base toString method
            }

        }

        public override void Move()
        {
            Console.WriteLine("The fish likes to swim"); //Added function from interface class 
        }
    }
}
