﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask10
{
    public interface IMovement // Interface that has a method that is needed to be implemented
    {
        public void Move();
      
    }
}
