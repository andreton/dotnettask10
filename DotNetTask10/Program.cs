﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace DotNetTask10
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<Animal> animalStack = new Stack<Animal>(); //Putting the animals in to a stack
            Console.WriteLine("You can either choose a default collection of animals, or create your own. Press (1) to make your own or (2) to use a custom collection");
            string userInput = Console.ReadLine();
            if (userInput == "2")
            {
                printStack(addAnimalsAutomatically());
                
            }
            else if (userInput == "1")
            {
                bool addingAnimals = true;
                while (addingAnimals)
                {
                    Console.WriteLine("Enter if you want to make a new bird or fish, (b) for bird or (f) for fish, press (e) when you are done with adding animals");
                    string birdOrFish = Console.ReadLine();
                    if (birdOrFish == "e")
                    {
                        addingAnimals = false;
                    }
                    else
                    {
                        animalStack.Push(addAnimalsManually(birdOrFish));
                    }
                }
                printStack(animalStack);
              
            }
            // Here are The logics for filtering: 
            Console.WriteLine("Do you wish to search for name(n) or color(c), please specify");
            var nameOrColor = Console.ReadLine();
            if (nameOrColor == "n")
            {
                linqSearchName(addAnimalsAutomatically());
            }
            else if (nameOrColor == "c")
            {
                linqSearchColor(addAnimalsAutomatically());

            }
            else
            {
                Console.WriteLine("Not legal try again");
            }

            //********************** Final logic for filtering using Linq********************************************// 
        }
        private static Stack<Animal> addAnimalsAutomatically()
        {
            Stack<Animal> autoAnimalStack = new Stack<Animal>();
            Animal bird1 = new Bird("White-mountain-Owl", "North-America", "white"); //  Making some animal object
            autoAnimalStack.Push(bird1);
            Animal bird2 = new Bird("Crow", "Nepal", "Black");
            autoAnimalStack.Push(bird2);
            Animal fish1 = new Fish("Tuna", "South Korea", "grey", "large"); // Making an owl with overridden constructor
            autoAnimalStack.Push(fish1);
            Animal fish2 = new Fish("Blue whale", "India", "Blue", "huge");
            autoAnimalStack.Push(fish2);
            Animal fish3 = new Fish("Sardina", "10", "12");
            autoAnimalStack.Push(fish3);
            return autoAnimalStack;
        }
        private static Animal addAnimalsManually(string animalChooser)
        {
            //These attributes are from the main class, and are needed both for fish and bird 
            Console.WriteLine("Enter the name of the Animal:");
            string name = Console.ReadLine();
            Console.WriteLine("Enter the country of origin:");
            string orginCountry = Console.ReadLine();
            Console.WriteLine("Enter the color:");
            string color = Console.ReadLine();
            if (animalChooser == "b") //If animal is bird
            {
                Console.WriteLine("Enter wingspan: ");
                string wingspan=Console.ReadLine();
                Animal bird = new Bird(name, orginCountry, color, wingspan);
                return bird;
                
            }
            else if(animalChooser== "f")//If animal is fish
            {
                Console.WriteLine("Enter weight: ");
                string weight = Console.ReadLine();
                Animal fish = new Fish(name, orginCountry, color, weight);
                return fish;
            }
            else
            {
                Console.WriteLine("That is no legal input"); //error handling
                return null;
            }

        }
        private static void printStack(Stack<Animal> inputStack)
        {
            foreach (var stackItem in inputStack) //iterate through the items in the stack 
            {
                Console.WriteLine(stackItem.ToString());
            }
        }

        private static void linqSearchName(Stack<Animal> inputStack) //Using Linq to filter based on name
        {
            Console.WriteLine("Enter which name you want to search for");
            string inputName=Console.ReadLine();
            IEnumerable<Animal> result = from results in inputStack //Using linq to filter
                                         where results.name == inputName
                                         orderby results descending
                                         select results;

            foreach (var i in result)
            {
                Console.WriteLine(i.ToString());
            }
        }
        private static void linqSearchColor(Stack<Animal> inputStack) // Using Linq to filter based on color
        {
            Console.WriteLine("Enter which color you want to search for");
            string inputColor = Console.ReadLine();
            IEnumerable<Animal> result = from results in inputStack //Using linq to filter
                                         where results.color == inputColor
                                         orderby results descending
                                         select results;

            foreach (var i in result)
            {
                Console.WriteLine(i.ToString());
            }
        }
    }
}

