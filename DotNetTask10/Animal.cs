﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection.Metadata;
using System.Text;

namespace DotNetTask10
{
    public abstract class Animal: IMovement //Abstract animal class where the different subclasses of animal can inherit the base cases
    {
        public string name { get; set; }
        public string orginCountry { get; set; }
        public string color { get; set; }

       


        public Animal() //Default constructor
        {

        }

        public Animal(string name, string orginCountry) //Override constructor
        {
            this.name = name;
            this.orginCountry = orginCountry;
        }
        public Animal(string name,string orginCountry, string color) //Extra constructor if animal class is to be made with color
        {
            this.name = name;
            this.orginCountry = orginCountry;
            this.color = color;
        }

        public override string ToString()
        {
            if (color == null)
            {
                return this.name+" : OrginCountry: " + this.orginCountry;
            }
            else
            {
                return this.name + " " + ": OrginCountry: " + this.orginCountry + ": color: " + this.color;
            }
          
        }
        public string runSwimFly(string input) //Method for declaring if animal flies, swims or runs
        {
           
            if (input == "running")
            {
                return "This animal loves to run";
            }
            else if (input == "flying")
            {
                return "this animal loves to run";
            }
            else if (input == "swim")
            {
                return "This animal loves to swim";
            }
            else
            {
                return "This animal loves to do nothing, it is probably a cat";
            }
        }
        public void isFishNemo()
        {
            Console.WriteLine("Is the fish starring the main role in Finding Nemo? Yes(y) No(n)");
            string variabel = Console.ReadLine();
            if (variabel == "y" || variabel == "Y")
            {
                Console.WriteLine("It is Nemo");
            }
            else if (variabel == "n" || variabel == "N")
            {
                Console.WriteLine("It is not Nemo :(");
            }
            else
            {
                Console.WriteLine("Please enter either (y) or (n)");
            }
        }

        public virtual void Move()
        {
            Console.WriteLine("All animals can move");        }
    }

}
